# Interactive Image CYO

A project to create a dynamic site to load settings and data in JSON format for Interactive Image CYOs or to generate a "static" site to be hosted on its own.

### Makes us of...

Projects and tech used:

* [Deno.Land] - A secure runtime for JavaScript and TypeScript.
* [Bulma] - A wonderful free, open source CSS framework.
* JSON - Markdown [[df1]] - 

### Todos

 - Write MORE Tests

License
----

???


[//]: # (These are reference links used in the body of this note and get stripped out when the markdown processor does its job. There is no need to format nicely because it shouldn't be seen. Thanks SO - http://stackoverflow.com/questions/4823468/store-comments-in-markdown-syntax)


   [df1]: <http://daringfireball.net/projects/markdown/>
   [Bulma]: <https://bulma.io/>
   [Deno.Land]: <https://deno.land/>

